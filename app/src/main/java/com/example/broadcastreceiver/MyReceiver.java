package com.example.broadcastreceiver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.util.Log;

public class MyReceiver extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {
        Log.d("asdf","Receiver onReceive()");

        if (intent.getAction().equals(Intent.ACTION_BOOT_COMPLETED)) {
            Intent i = new Intent(context, TestMyService.class);
            if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.O){
                Log.d("asdf","Receiver startForegroundService()");
                context.startForegroundService(i);
            }
            else{
                Log.d("asdf","Receiver startService()");
                context.startService(i);
            }
        }

    }
}
