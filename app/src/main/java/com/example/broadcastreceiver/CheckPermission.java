package com.example.broadcastreceiver;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.Manifest;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

public class CheckPermission extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_check_permission);
        Log.d("asdf", "check permissions onCreate()");

        String[] permission_list = {
                Manifest.permission.ACCESS_COARSE_LOCATION
        };

        for(String permission : permission_list){
            if(checkCallingOrSelfPermission(permission) == PackageManager.PERMISSION_DENIED){
                Log.d("asdf", " checkCallingOrSelfPermission(permission)");
                requestPermissions(permission_list,123);
            }
            else{
                Log.d("asdf", " finish()");
                finish();
            }
        }

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        Log.d("asdf","in onRequestPermissionsResult()");
        if(requestCode==123)
        {
            for(int i=0; i<grantResults.length; i++)
            {
                if(grantResults[i]==PackageManager.PERMISSION_GRANTED){
                    Log.d("asdf", "check permissions");
                }
                else {
                    Toast.makeText(getApplicationContext(),"Need permission", Toast.LENGTH_LONG).show();
                    //finish();
                }
            }
        }
        finish();
    }

}
