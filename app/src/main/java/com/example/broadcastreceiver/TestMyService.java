package com.example.broadcastreceiver;

import android.Manifest;
import android.app.AlarmManager;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothGatt;
import android.bluetooth.BluetoothGattCallback;
import android.bluetooth.BluetoothGattCharacteristic;
import android.bluetooth.BluetoothGattDescriptor;
import android.bluetooth.BluetoothGattService;
import android.bluetooth.BluetoothManager;
import android.bluetooth.BluetoothProfile;
import android.bluetooth.le.BluetoothLeScanner;
import android.bluetooth.le.ScanCallback;
import android.bluetooth.le.ScanResult;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.IBinder;
import android.util.Log;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.app.AppCompatCallback;
import androidx.core.app.NotificationCompat;

import java.util.Calendar;
import java.util.UUID;

import static android.bluetooth.BluetoothProfile.STATE_CONNECTED;

public class TestMyService extends Service {
    public static Intent serviceIntent = null;

    private BluetoothManager bluetoothManager;
    private BluetoothAdapter bluetoothAdapter;
    private BluetoothLeScanner bluetoothLesScanner;
    private BluetoothGatt bluetoothGatt;
    final static UUID SERVICE_UUID = UUID.fromString("6e400001-b5a3-f393-e0a9-e50e24dcca9e");
    final static UUID CHARACTERISTIC_UUID = UUID.fromString("6e400003-b5a3-f393-e0a9-e50e24dcca9e");
    final static UUID CLIENT_CHARACTERISTIC_CONFIG_UUID = UUID.fromString("00002902-0000-1000-8000-00805f9b34fb");




    private BluetoothGattCallback myGattCallback = new BluetoothGattCallback(){
        @Override
        public void onConnectionStateChange(BluetoothGatt gatt, int status, int newState) {
            super.onConnectionStateChange(gatt, status, newState);

            if (newState == STATE_CONNECTED) {

                Log.i("asdf", "Connected to GATT server.");
                Log.i("asdf", "Attempting to start service discovery:" +
                        bluetoothGatt.discoverServices());

            } else if (newState == BluetoothProfile.STATE_DISCONNECTED) {

                Log.i("asdf", "Disconnected from GATT server.");
            }
        }

        @Override
        public void onServicesDiscovered(BluetoothGatt gatt, int status) {
            super.onServicesDiscovered(gatt, status);
            if(status == BluetoothGatt.GATT_SUCCESS){
                Log.d("asdf","IN SUCCESS");
                BluetoothGattService service = gatt.getService(SERVICE_UUID);
                if(service != null){
                    Log.d("asdf","SERVICE");
                    BluetoothGattCharacteristic characteristic = service.getCharacteristic(CHARACTERISTIC_UUID);
                    if(characteristic != null){
                        Log.d("asdf","CHARACTERISTIC");
                        int properties = characteristic.getProperties();
                        if((properties|BluetoothGattCharacteristic.PROPERTY_NOTIFY)>0){
                            gatt.setCharacteristicNotification(characteristic,true);
                        }
                        BluetoothGattDescriptor descriptor = characteristic.getDescriptor(CLIENT_CHARACTERISTIC_CONFIG_UUID);
                        descriptor.setValue(BluetoothGattDescriptor.ENABLE_NOTIFICATION_VALUE);
                        gatt.writeDescriptor(descriptor);
                        Log.d("asdf","test");
                    }
                }

            }
            else{
                Log.d("asdf","FAILED");
            }
        }

        @Override
        public void onCharacteristicChanged(BluetoothGatt gatt, BluetoothGattCharacteristic characteristic) {
            super.onCharacteristicChanged(gatt, characteristic);
            if(characteristic.getUuid().equals(CHARACTERISTIC_UUID)){
                final byte[] data = characteristic.getValue();
                if(data != null && data.length > 0){
                    final StringBuilder stringBuilder = new StringBuilder(data.length);
                    for(byte byteChar : data)
                        stringBuilder.append(String.format("%02X ",byteChar));
                    Log.d("asdf",stringBuilder.toString());
                }
            }

        }
    };

    private ScanCallback myScanCallback = new ScanCallback() {
        @Override
        public void onScanResult(int callbackType, ScanResult result) {
            super.onScanResult(callbackType, result);
            String name = result.getDevice().getName();
            Log.d("asdf",result + "");
            if("HUSTAR_11".equals(name)){
                bluetoothGatt = result.getDevice().connectGatt(getApplicationContext(), false, myGattCallback);
                bluetoothLesScanner.stopScan(myScanCallback);
            }
        }

        @Override
        public void onScanFailed(int errorCode) {
            super.onScanFailed(errorCode);
            Log.d("asdf","Scan Failed");
        }
    };

    public TestMyService() {
        Log.d("asdf","TestMyservice()");


    }

    /**************************************************************************************/
    @Override
    public void onCreate() {
        super.onCreate();
        Log.d("asdf","onCreate()");


    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        Log.d("asdf","onDestroy()");

    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Log.d("asdf","onStartCommand()");
        bluetoothManager = (BluetoothManager) this.getApplicationContext().getSystemService(BLUETOOTH_SERVICE);
        bluetoothAdapter = bluetoothManager.getAdapter();
        bluetoothLesScanner = bluetoothAdapter.getBluetoothLeScanner();


        serviceIntent = intent;
        initializeNotification();

        intent = new Intent(this, CheckPermission.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);


        Log.d("asdf","END onStartCommand");
        bluetoothLesScanner.startScan(myScanCallback);





        return START_STICKY;
    }

    @Override
    public IBinder onBind(Intent intent) {
        // TODO: Return the communication channel to the service.
        throw new UnsupportedOperationException("Not yet implemented");
    }

    public void initializeNotification() {
        Log.d("asdf","in initializeNotification()");
        NotificationCompat.Builder builder = new NotificationCompat.Builder(this, "1");
        builder.setSmallIcon(R.mipmap.ic_launcher);
        NotificationCompat.BigTextStyle style = new NotificationCompat.BigTextStyle();
        style.bigText("설정을 보려면 누르세요.");
        style.setBigContentTitle(null);
        style.setSummaryText("HUSTAR 서비스 동작중");
        builder.setContentText(null);
        builder.setContentTitle(null);
        builder.setOngoing(true);
        builder.setStyle(style);
        builder.setWhen(0);
        builder.setShowWhen(false);
        Intent notificationIntent = new Intent(this, MainActivity.class);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, notificationIntent, 0);
        builder.setContentIntent(pendingIntent);
        NotificationManager manager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        try{
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                manager.createNotificationChannel(new NotificationChannel("1", "undead_service", NotificationManager.IMPORTANCE_NONE));
            }
        }catch(SecurityException e){
            Log.d("asdf","SecurityException");
        }

        Notification notification = builder.build();
        startForeground(1, notification);
    }

}
